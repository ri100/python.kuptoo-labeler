import csv, os
import pickle
from collections import defaultdict
from annotator_dbs.storage import indices
from annotator_enums.enums import Key

prod2idx = indices[Key.PRODUKT1]


def get_key_and_label(token):
    return token.split('|')


def label_2_index(files):
    labels = defaultdict(list)
    labels_counts = defaultdict(int)
    for file in files:
        with open(file) as f:
            r = csv.reader(f)
            for row in r:
                if row[1]=='*':
                    continue
                for x in row[4:]:
                    if not x:
                        continue
                    for token in x.split(','):
                        token = token.strip().lower()
                        key, label = get_key_and_label(token)
                        labels[key].append(label)
                        labels_counts[token] += 1

    labels = {key: list(set(data)) for key, data in labels.items()}
    return labels, labels_counts


def label_2_dict(file, pickle_folder):
    labels, labels_counts = label_2_index([file])

    key2label2idx = {}
    for key, label in labels.items():
        key2label2idx[key] = {v: k + 1 for k, v in enumerate(sorted(label))}

    for key, token in key2label2idx.items():
        key2label2idx[key][""] = 0

    print(key2label2idx)
    print(key2label2idx.keys())
    with open('{}/key2label2index.pickle'.format(pickle_folder), 'wb') as f:
        pickle.dump(key2label2idx, f, pickle.HIGHEST_PROTOCOL)

    label2idx = {}
    with open("{}/key2label2idx.csv".format(pickle_folder), 'w') as f:
        wr = csv.writer(f)
        i = 0
        for key, d in key2label2idx.items():
            for token, idx in d.items():
                if token:
                    label2idx["{}|{}".format(key, token)] = i
                    i += 1
                    row = [key, token, idx]
                    wr.writerow(row)

    print(label2idx)
    with open('{}/label2index.pickle'.format(pickle_folder), 'wb') as f:
        pickle.dump(label2idx, f, pickle.HIGHEST_PROTOCOL)
