import os

from kuptoo_labeler_trainer.utils.create_label_dict import label_2_dict

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
# os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
# os.environ['WANDB_MODE'] = 'dryrun'
from collections import OrderedDict
from data_utils.encoder_list import EncoderList
from data_utils.encoders.binary_encoder import BinaryEncoder
from data_utils.encoders.word2vec_encoder import Word2VecEncoder
from data_utils.readers.json_reader import json_reader
from kuptoo_labeler.ml.utils.serializer import load_obj
from tensorflow.keras.layers import Conv1D
from kuptoo_labeler_trainer.utils.project_folder import get_project_folder
from tensorflow.keras import Input, Model
from tensorflow.keras.layers import Dense, Flatten, Concatenate, RepeatVector
import wandb
from wandb.keras import WandbCallback
from tensorflow.keras.callbacks import ModelCheckpoint
from tensorflow.python.keras.optimizer_v2.adam import Adam
from data_utils.generator import Generator
import tensorflow as tf

local_folder = os.path.dirname(__file__)
pickle_folder = os.path.join(local_folder, 'pickle')
input_models_folder = os.path.join(local_folder, 'models/input')

dataset_name = "dataset-23"
pickle_dataset_folder = os.path.join(pickle_folder, dataset_name)

if not os.path.isdir(pickle_dataset_folder):
    os.mkdir(pickle_dataset_folder)

# create dicts

label_2_dict('files/{}/implicit_labels.csv'.format(dataset_name), pickle_dataset_folder)

# load dicts

key2label2index = load_obj(os.path.join(pickle_dataset_folder, 'key2label2index.pickle'))
key2label2index_dim = {k: max(d.values()) + 1 for k, d in key2label2index.items()}

label2key_and_index = {}
for key, data in key2label2index.items():
    for label, index in data.items():
        # if label in label2key_and_index:
        #     print(label)
        label2key_and_index[label] = (key, index)


def _get_checkpoint(project, run_name):
    folder = get_project_folder(project, run_name)
    model_checkpoint = '%s/%s.e-{epoch:02d}.vl-{val_loss:.4f}.l-{loss:.8f}.h5' % (folder, run_name)
    return ModelCheckpoint(model_checkpoint, save_best_only=False, save_weights_only=False, verbose=1)


def _get_generators(inputs, outputs):
    def _data_reader(file):
        while True:
            for data in json_reader(file):
                title = data['title']['std']
                output = OrderedDict({
                    'context': [],
                    'loc': [],
                    'products': [],
                    'action': [],
                    'do': [],
                    'dla': [],
                    'na': [],
                    'time': [],
                    'set': []
                })

                # don't train records with no labels
                if not data['labels']:
                    continue

                for key, labels in data['labels'].items():
                    for label in labels:
                        try:
                            index = key2label2index[key][label]
                            output[key].append(index)
                        except KeyError:
                            print('Error NO KEY {} or LABEL {}'.format(key, label), data['labels'])
                            continue

                yield title, output

    train_generator = Generator(
        data=_data_reader(config.train_file),
        input_encoders=inputs,
        output_encoders=outputs
    )

    eval_generator = Generator(
        data=_data_reader(config.eval_file),
        input_encoders=inputs,
        output_encoders=outputs
    )

    return train_generator, eval_generator


def get_model(inputs, outputs):
    i1 = Input(inputs['input'].shape())

    cnn_1 = Conv1D(200, 3, padding='valid', activation='relu', strides=1)(i1)
    cnn_2 = Conv1D(128, 2, padding='valid', activation='relu', strides=1)(cnn_1)
    flat_5 = Flatten()(cnn_2)
    cnn_3 = Dense(300, activation="relu", name="latient")(flat_5)

    outputs = [Dense(encoder.dim(), activation="sigmoid", name=name)(cnn_3)
               for name, encoder in outputs.items()]

    return Model(inputs=[i1], outputs=outputs)


# def get_model_10(sub_model, inputs, outputs):
#     i1 = Input(inputs['input'].shape())
#
#     sub_space = sub_model(i1)
#
#     cnn_1 = Conv1D(200, 3, padding='valid', activation='relu', strides=1)(i1)
#     cnn_2 = Conv1D(128, 2, padding='valid', activation='relu', strides=1)(cnn_1)
#     flat = Flatten()(cnn_2)
#     concat = Concatenate()([sub_space, flat])
#     dens_1 = Dense(300, activation="relu", name="latient")(concat)
#
#     outputs = [Dense(encoder.dim(), activation="sigmoid", name=name)(dens_1)
#                for name, encoder in outputs.items()]
#
#     return Model(inputs=[i1], outputs=outputs)


# def get_model_11(sub_model, inputs, outputs):
#     i1 = Input(inputs['input'].shape())
#
#     sub_space = sub_model(i1)
#
#     def cnn_rel():
#         pass
#
#     def cnn(i1, sub_space):
#         cnn_1 = Conv1D(200, 3, padding='valid', activation='relu', strides=1)(i1)
#         cnn_2 = Conv1D(128, 2, padding='valid', activation='relu', strides=1)(cnn_1)
#         flat = Flatten()(cnn_2)
#         return Concatenate()([sub_space, flat])
#
#     concat_1 = cnn(i1, sub_space)
#     concat_2 = cnn(i1, sub_space)
#     concat_3 = cnn(i1, sub_space)
#     concat_4 = cnn(i1, sub_space)
#
#     dens = {
#         'context': Dense(230, activation="relu")(concat_1),
#         'products': Dense(230, activation="relu")(concat_2),
#         'action': Dense(230, activation="relu")(concat_3),
#         'rel': Dense(230, activation="relu")(concat_4),
#     }
#
#     outputs = [Dense(encoder.dim(), activation="sigmoid", name=name)(dens[name])
#                for name, encoder in outputs.items()]
#
#     return Model(inputs=[i1], outputs=outputs)


def get_model_12(prod_feature_do_model, rel_sub_model, rel_sub_model_ext, inputs, outputs):
    i1 = Input(inputs['input'].shape())

    prod_feature_do_vector = prod_feature_do_model(i1)

    def cnn(i1):
        enc_input = rel_sub_model(i1)
        enc_input_ext = rel_sub_model_ext(i1)

        cnn_1 = Conv1D(200, 3, padding='valid', activation='relu', strides=1)(i1)
        cnn_2 = Conv1D(128, 2, padding='valid', activation='relu', strides=1)(cnn_1)
        flat = Flatten()(cnn_2)
        return Concatenate()([flat, prod_feature_do_vector, enc_input, enc_input_ext])

    concat_context = cnn(i1)
    concat_loc = cnn(i1)
    concat_products = cnn(i1)
    concat_action = cnn(i1)
    concat_do = cnn(i1)
    concat_dla = cnn(i1)
    concat_na = cnn(i1)
    concat_time = cnn(i1)
    concat_set = cnn(i1)

    dens = {
        'context': Dense(300, activation="relu")(concat_context),
        'loc': Dense(64, activation="relu")(concat_loc),
        'products': Dense(400, activation="relu")(concat_products),
        'action': Dense(164, activation="relu")(concat_action),
        'do': Dense(400, activation="relu")(concat_do),
        'dla': Dense(64, activation="relu")(concat_dla),
        'na': Dense(64, activation="relu")(concat_na),
        'time': Dense(64, activation="relu")(concat_time),
        'set': Dense(64, activation="relu")(concat_set),
    }

    outputs = [Dense(encoder.dim(), activation="sigmoid", name=name)(dens[name])
               for name, encoder in outputs.items()]

    return Model(inputs=[i1], outputs=outputs)


# def get_model_13(prod_feature_do_model, rel_sub_model, rel_sub_model_ext, inputs, outputs):
#     i1 = Input(inputs['input'].shape())
#
#     prod_feature_do_vector = prod_feature_do_model(i1)
#
#     def cnn(i1):
#         enc_input = rel_sub_model(i1)
#         enc_input_ext = rel_sub_model_ext(i1)
#
#         i2 = RepeatVector(16)(prod_feature_do_vector)
#         i3 = Concatenate()([i1, i2])
#         cnn_1 = Conv1D(200, 3, padding='valid', activation='relu', strides=1)(i3)
#         cnn_2 = Conv1D(128, 2, padding='valid', activation='relu', strides=1)(cnn_1)
#         flat = Flatten()(cnn_2)
#         return Concatenate()([flat, prod_feature_do_vector, enc_input, enc_input_ext])
#
#     concat_context = cnn(i1)
#     concat_loc = cnn(i1)
#     concat_products = cnn(i1)
#     concat_action = cnn(i1)
#     concat_do = cnn(i1)
#     concat_dla = cnn(i1)
#     concat_na = cnn(i1)
#     concat_time = cnn(i1)
#
#     dens = {
#         'context': Dense(300, activation="relu")(concat_context),
#         'loc': Dense(64, activation="relu")(concat_loc),
#         'products': Dense(400, activation="relu")(concat_products),
#         'action': Dense(164, activation="relu")(concat_action),
#         'do': Dense(400, activation="relu")(concat_do),
#         'dla': Dense(64, activation="relu")(concat_dla),
#         'na': Dense(64, activation="relu")(concat_na),
#         'time': Dense(64, activation="relu")(concat_time),
#
#     }
#
#     outputs = [Dense(encoder.dim(), activation="sigmoid", name=name)(dens[name])
#                for name, encoder in outputs.items()]
#
#     return Model(inputs=[i1], outputs=outputs)


# def get_model_14(prod_feature_do_model, rel_sub_model, rel_sub_model_ext, inputs, outputs):
#     i1 = Input(inputs['input'].shape())
#
#     prod_feature_do_vector = prod_feature_do_model(i1)
#
#     def cnn(i1):
#         enc_input = rel_sub_model(i1)
#         enc_input_ext = rel_sub_model_ext(i1)
#
#         i2 = RepeatVector(16)(prod_feature_do_vector)
#
#         cnn_1 = Conv1D(200, 3, padding='same', activation='relu', strides=1)(i1)
#         i3 = Concatenate()([cnn_1, i2])
#         cnn_2 = Conv1D(128, 2, padding='valid', activation='relu', strides=1)(i3)
#         flat = Flatten()(cnn_2)
#         return Concatenate()([flat, prod_feature_do_vector, enc_input, enc_input_ext])
#
#     concat_context = cnn(i1)
#     concat_loc = cnn(i1)
#     concat_products = cnn(i1)
#     concat_action = cnn(i1)
#     concat_do = cnn(i1)
#     concat_dla = cnn(i1)
#     concat_na = cnn(i1)
#     concat_time = cnn(i1)
#
#     dens = {
#         'context': Dense(300, activation="relu")(concat_context),
#         'loc': Dense(64, activation="relu")(concat_loc),
#         'products': Dense(400, activation="relu")(concat_products),
#         'action': Dense(164, activation="relu")(concat_action),
#         'do': Dense(400, activation="relu")(concat_do),
#         'dla': Dense(64, activation="relu")(concat_dla),
#         'na': Dense(64, activation="relu")(concat_na),
#         'time': Dense(64, activation="relu")(concat_time),
#
#     }
#
#     outputs = [Dense(encoder.dim(), activation="sigmoid", name=name)(dens[name])
#                for name, encoder in outputs.items()]
#
#     return Model(inputs=[i1], outputs=outputs)


default_config = dict(
    max_words=16,
    batch=1500,
    train_file=f"/mnt/data/corpus_annotated/allegro/{dataset_name}/train.json",
    eval_file=f"/mnt/data/corpus_annotated/allegro/{dataset_name}/eval.json",
    optimizer='adam',
    learning_rate=0.00075,
    steps_per_epoch=100,
    validation_steps=30,
    epochs=1250,
    dataset_name=dataset_name,
)
project = "labeler"
wandb.init(config=default_config, project=project, tags=['feasible-sun', 'model-12', '_-extension'])
wandb.run.save()
run_name = wandb.run.name
config = wandb.config

inputs = EncoderList({
    'input': Word2VecEncoder(config.max_words, skip_empty=False),
})
outputs = EncoderList({
    'context': BinaryEncoder(dim=key2label2index_dim['context']),
    'loc': BinaryEncoder(dim=key2label2index_dim['loc']),
    'products': BinaryEncoder(dim=key2label2index_dim['products']),
    'action': BinaryEncoder(dim=key2label2index_dim['action']),
    'do': BinaryEncoder(dim=key2label2index_dim['do']),
    'dla': BinaryEncoder(dim=key2label2index_dim['dla']),
    'na': BinaryEncoder(dim=key2label2index_dim['na']),
    'time': BinaryEncoder(dim=key2label2index_dim['time']),
    'set': BinaryEncoder(dim=key2label2index_dim['set']),
})

train_generator, eval_generator = _get_generators(inputs, outputs)

# for x,y in train_generator:
#     pass
#     # print(x[0].shape)
#     # print(y[0].shape)
#     # print(y[1].shape)
#     # print(y[2].shape)
#     # print(y[3])
#     # print(y[4])

input_shapes, output_shapes = train_generator.shapes()

types = train_generator.types()

train_dataset = tf.data.Dataset.from_generator(train_generator, output_types=types).repeat().batch(
    config.batch).prefetch(-1)
eval_dataset = tf.data.Dataset.from_generator(eval_generator, output_types=types).repeat().batch(config.batch).prefetch(
    -1)

# rel_model_ext
w = os.path.join(input_models_folder, 'labeler/stilted-deluge-66.e-05.vl-0.0001.l-0.00012881.h5')
rel_model_ext = tf.keras.models.load_model(w, custom_objects={})
print('rel_model_ext')
print(rel_model_ext.summary())
latient = rel_model_ext.get_layer('dense').output
rel_model_ext = Model(inputs=rel_model_ext.inputs, outputs=latient)
rel_model_ext.trainable = False

# rel_model
w = os.path.join(input_models_folder, 'labeler/skilled-disco-77.e-185.vl-0.0026.l-0.00255457.h5')
rel_model = tf.keras.models.load_model(w, custom_objects={})
print('rel_model')
print(rel_model.summary())
latient = rel_model.get_layer('dense').output
rel_model = Model(inputs=rel_model.inputs, outputs=latient)
rel_model.trainable = False

w = os.path.join(input_models_folder, 'labeler-grouper/feasible-sun-53.e-332.vl-2.3891.l-2.29498757.h5')
sub_model = tf.keras.models.load_model(w, custom_objects={})
print('sub_model')
print(sub_model.summary())
latient = sub_model.get_layer('latient').output
sub_model = Model(inputs=sub_model.inputs, outputs=latient)
sub_model.trainable = False

model = get_model_12(sub_model, rel_model, rel_model_ext, inputs, outputs)
# model.load_weights('projects/labeler/fallen-paper-162/fallen-paper-162.e-250.vl-0.0302.l-0.03012385.h5')
adam = Adam(learning_rate=config.learning_rate)

losses = {
    "context": "binary_crossentropy",
    "loc": "binary_crossentropy",
    "products": "binary_crossentropy",
    "action": "binary_crossentropy",
    "do": "binary_crossentropy",
    "dla": "binary_crossentropy",
    "na": "binary_crossentropy",
    "time": "binary_crossentropy",
    "set": "binary_crossentropy"
}
loss_weights = {
    "context": 1.0,
    "loc": 1.0,
    "products": 1.0,
    "action": 1.0,
    "do": 1.0,
    "dla": 1.0,
    "na": 1.0,
    "time": 1.0,
    "set": 1.0
}

model.compile(loss=losses, loss_weights=loss_weights, optimizer=adam, metrics=['acc'])
print(model.summary())

cp = _get_checkpoint(project, run_name)
model.fit_generator(train_dataset, validation_data=eval_dataset,
                    steps_per_epoch=config.steps_per_epoch, validation_steps=config.validation_steps,
                    epochs=config.epochs, callbacks=[cp, WandbCallback()])
