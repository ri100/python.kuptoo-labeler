from setuptools import setup

setup(
    name='kuptoo_labeler',
    version='0.7',
    description='Product description labeler',
    author='Risto Kowaczewski',
    author_email='risto.kowaczewski@gmail.com',
    packages=['kuptoo_labeler'],
    install_requires=[
        'data-utils @ git+https://ri100@bitbucket.org/ri100/python.dataset.git#egg=data-utils',
        'numpy',
        'tensorflow==2.2.0',
    ],
    include_package_data=True
)
