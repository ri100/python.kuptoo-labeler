import os
import numpy as np
import tensorflow as tf
from collections import defaultdict
from data_utils.encoders.word2vec_encoder import Word2VecEncoder
from kuptoo_labeler.ml.utils.serializer import load_obj

local_dir = os.path.dirname(__file__)


class LabelerModel:
    def __init__(self, model_path: str = None):
        self.key2label2index = load_obj(os.path.join(local_dir, '../pickle/dataset-20/key2label2index.pickle'))
        self.key2index2label = self._prepare_key2index2label()

        if model_path is None:
            model_path = os.path.join(
                local_dir,
                '../ml/dataset-20/young-vortex-166/young-vortex-166.e-1243.vl-0.0260.l-0.02590996.h5')

        self.model = tf.keras.models.load_model(model_path)

        self.max_words_in_title = 16
        self.word2vec_encoder = Word2VecEncoder(self.max_words_in_title, skip_empty=False)

    def _prepare_key2index2label(self):
        key2index2label = {}
        for key, data in self.key2label2index.items():
            key2index2label[key] = {v: k for k, v in data.items()}
        return key2index2label

    def summary(self):
        print(self.model.summary())

    def predict(self, x):
        x = self._encode_input(x)
        y_pred = self.model.predict(x)
        x_len = len(x)
        return self._covert_output(y_pred, x_len)

    def _encode_input(self, texts):
        def _encode(text):
            return self.word2vec_encoder.encode(text)

        return np.array([_encode(text) for text in texts])

    def _covert_output(self, y_pred, x_len, output_thresholds=None):

        def _get_structured_data(x_position):
            result = defaultdict(list)
            for key_position, (key, min_prob) in enumerate(output_thresholds.items()):
                output_pred = y_pred[key_position][x_position]
                output_pred = sorted([(p, y) for p, y in enumerate(output_pred)], reverse=True, key=lambda x: x[1])
                for p, prob in output_pred:
                    if prob > min_prob:
                        label = self.key2index2label[key][p]
                        result[key].append((label, prob.item()))
                    else:
                        break
            return result

        if output_thresholds is None:
            output_thresholds = {'context': .2,
                                 'loc': .2,
                                 'products': .2,
                                 'action': .2,
                                 'do': .2,
                                 'dla': .2,
                                 'na': .2,
                                 'time': .2}

        for text_position in range(x_len):
            yield _get_structured_data(text_position)

