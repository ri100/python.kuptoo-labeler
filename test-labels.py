import os
from pprint import pprint
from data_utils.nlp.standardizers import StandardizedText
from kuptoo_labeler.ml.labler_model import LabelerModel

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

texts = ["Gryf łamany 120cm 55 kg sztanga obciążenie HANTLE", "Narty Dynastar My First girl 67 cm Narty dziecięce.", "Harley Davidson Softail Vance & Hines wydech tłumik "]

l = LabelerModel()
texts = [str(StandardizedText(text.lower()).standardize()) for text in texts]
pprint(list(l.predict(texts)))
