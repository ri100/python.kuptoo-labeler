import os

local_path = os.path.dirname(__file__)


def get_project_folder(project, run_name):
    folder = f"../projects/{project}"
    folder = os.path.join(local_path, folder)
    if not os.path.exists(folder):
        os.mkdir(folder)
    folder = f"{folder}/{run_name}"
    if not os.path.exists(folder):
        os.mkdir(folder)
    return folder


def get_project_dataset_folder(project, dataset_name, run_name):
    folder = f"../projects/{dataset_name}/{project}"
    folder = os.path.join(local_path, folder)
    if not os.path.exists(folder):
        os.mkdir(folder)
    folder = f"{folder}/{run_name}"
    if not os.path.exists(folder):
        os.mkdir(folder)
    return folder
